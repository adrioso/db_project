<!DOCTYPE html>
<html>
    <head>
        <title>Gestionar administradores HERR-APP</title>
        <link rel="stylesheet" type="text/css" href="style.css" >
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="titulo">
            <div>
                <br>
                <h1 align="center" style="color: white">GESTIONAR ADMINISTRADORES</h1>
                <h2 align="center" style="color: white">Menu</h2>
            </div>

            <div class="scrollmenu">
                <a href="/db-project/index.html">Inicio</a>
                <a href="/db-project/admin/FORMS/registrar-admin-form.html">Registrar administrador</a>
                <a href="/db-project/admin/FORMS/eliminar-admin-form.php">Eliminar administrador</a>
                <a href="/db-project/admin/FORMS/consultar-admin-form.php">Consultar</a>
                <a href="/db-project/admin/FORMS/buscar-admin-form.php">Buscar</a>
            </div>
        </div>
        <div>
            <?php
                include($_SERVER['DOCUMENT_ROOT']."/db-project/admin/CRUD/admin-service.php");
                $nuevo = new Admin_Service();
                $nuevo -> mostrar_admin();
            ?>
        </div>
    </body>
</html>
