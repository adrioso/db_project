<?php
    require $_SERVER['DOCUMENT_ROOT'] ."\db-project\conexion.php" ;
    class Admin_Service{
        public function insertar_admin ($cedula, $nombre, $fecha_nacimiento, $tipo_de_sangre, $eps, $codigo_acceso ){
                $conne = Conectar::conn();
                $sql = "INSERT INTO administrador VALUES ('$cedula', '$nombre', '$fecha_nacimiento', '$tipo_de_sangre', '$eps', '$codigo_acceso')";
                if(!mysqli_query($conne, $sql)){
                    echo "Se ha producido un error al registrar el administrador <br>";
                    echo $conne -> errno ."=". $conne -> error ."<br>";
                    echo "<button onClick='history.back()'>Regresar</button>";
                }
                else{
                    echo    "<script type='text/javascript'>
                                alert('El administrador ha sido registrado correctamente');
                                window.history.go(-1);
                            </script>";
                }

        }

        public function mostrar_admin(){
            $conne = Conectar::conn();
            $sql = "SELECT cedula, nombre, fecha_nacimiento, tipo_de_sangre, eps, codigo_acceso FROM `administrador`";

            $datos = mysqli_query($conne, $sql);

            if(($conne -> error)){
                echo "Se ha producido un error al consultar la informacion de los administradores <br>";
                echo $conne -> errno ."=". $conne -> error ."<br>";
            }
            else{
                echo "<table>";
                    echo "<tr>";
                        echo "<td><b>Cedula</b></td>";
                        echo "<td><b>Nombre</b></td>";
                        echo "<td><b>Fecha de nacimiento</b></td>";
                        echo "<td><b>Tipo de sangre</b></td>";
                        echo "<td><b>EPS</b></td>";
                        echo "<td><b>Código de acceso</b></td>";
                    echo "</tr>";
                while ($fila =mysqli_fetch_array($datos)){
                    echo "<tr>";
                        echo "<td>".$fila ["cedula"]."</td>";
                        echo "<td>".$fila ["nombre"]."</td>";
                        echo "<td>".$fila ["fecha_nacimiento"]."</td>";
                        echo "<td>".$fila ["tipo_de_sangre"]."</td>";
                        echo "<td>".$fila ["eps"]."</td>";
                        echo "<td>".$fila ["codigo_acceso"]."</td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        }

        public function borrar_admin ($cedula){
            $conne = Conectar::conn();
            $sql = "DELETE administrador,  estandar 
                      FROM administrador 
                 LEFT JOIN estandar ON (administrador.cedula = estandar.cedula_jefe OR administrador.cedula = estandar.cedula_asistido)
                     WHERE administrador.cedula = $cedula";
                if(!mysqli_query($conne, $sql)){
                    echo "Se ha producido un error al registrar el administrador <br>";
                    echo $conne -> errno ."=". $conne -> error ."<br>";
                    echo "<button onClick='history.back()'>Regresar</button>";
                }
                else{
                    echo    "<script type='text/javascript'>
                                alert('El administrador ha sido borrado correctamente');
                                window.history.go(-1);
                            </script>";
                }
        }

        public function consultar_admin_sinasistente(){
            $conne = Conectar::conn();
            $sql = "SELECT cedula, nombre, fecha_nacimiento, tipo_de_sangre, eps, codigo_acceso 
                      FROM `administrador` a
                     WHERE NOT EXISTS (SELECT 'X' 
                                         FROM `estandar`
                                        WHERE cedula_asistido = a.cedula)";
            $datos = mysqli_query($conne, $sql);

            if(($conne -> error)){
                echo "Se ha producido un error al obtener la informacion de los administradores sin asistente <br>";
                echo $conne -> errno ."=". $conne -> error ."<br>";
            }
            else{
                echo "<table>";
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Cedula</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Nombre</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Fecha de nacimiento</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Tipo de sangre</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>EPS</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Código de acceso</b></td>";
                    echo "</tr>";
                while ($fila =mysqli_fetch_array($datos)){
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["cedula"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["nombre"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["fecha_nacimiento"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["tipo_de_sangre"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["eps"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["codigo_acceso"]."</td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        }

        public function consultar_admin_nrosupervisado(){
            $conne = Conectar::conn();
            $sql = "SELECT a.cedula cedula, a.nombre nombre, Count(*) numero
                      FROM `administrador` a, `estandar` b
                     WHERE a.cedula = b.cedula_asistido
                    GROUP BY a.cedula, a.nombre
                    UNION ALL
                    SELECT cedula, nombre, 0 numero
                      FROM `administrador` a
                     WHERE cedula NOT IN (SELECT DISTINCT a.cedula cedula
                                            FROM `administrador` a, `estandar` b
                                           WHERE a.cedula = b.cedula_asistido)
                    GROUP BY cedula, nombre";
            $datos = mysqli_query($conne, $sql);

            if(($conne -> error)){
                echo "Se ha producido un error al obtener la informacion de los administradores sin asistente <br>";
                echo $conne -> errno ."=". $conne -> error ."<br>";
            }
            else{
                echo "<table>";
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Cedula</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Nombre</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Número trabajadores estandar suepervisados</b></td>";
                    echo "</tr>";
                while ($fila =mysqli_fetch_array($datos)){
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["cedula"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["nombre"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["numero"]."</td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        }

        public function consultar_admin_mismaeps(){
            $conne = Conectar::conn();
            $sql = "SELECT admi.cedula cedula, admi.nombre nombre, admi.fecha_nacimiento fecha_nacimiento, admi.tipo_de_sangre tipo_de_sangre, admi.eps eps, admi.codigo_acceso codigo_acceso 
            FROM  administrador admi
                 ,estandar est
                 ,(SELECT cedulatabla1 cedulatabla2, count(*) cuenta
                     FROM( SELECT  a.cedula cedulatabla1, b.eps eps, count(*)
                             FROM `administrador` a, `estandar` b
                            WHERE a.cedula = b.cedula_asistido
                            GROUP BY a.cedula, b.eps
                         ) tabla1
                   GROUP BY cedulatabla1
                   HAVING cuenta = 1) tabla2
           WHERE tabla2.cedulatabla2 = admi.cedula
             AND est.cedula_asistido = admi.cedula
           GROUP BY admi.cedula, admi.nombre, admi.fecha_nacimiento, admi.tipo_de_sangre, admi.eps, admi.codigo_acceso
           HAVING count(est.cedula)>=2";
            $datos = mysqli_query($conne, $sql);

            if(($conne -> error)){
                echo "Se ha producido un error al obtener la informacion de los administradores sin asistente <br>";
                echo $conne -> errno ."=". $conne -> error ."<br>";
            }
            else{
                echo "<link rel='stylesheet' type='text/css' href='style.css'>";
                echo "<table>";
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Cedula</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Nombre</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Fecha de nacimiento</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Tipo de sangre</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>EPS</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Código de acceso</b></td>";
                    echo "</tr>";
                while ($fila =mysqli_fetch_array($datos)){
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["cedula"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["nombre"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["fecha_nacimiento"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["tipo_de_sangre"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["eps"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["codigo_acceso"]."</td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        }


    }
?>