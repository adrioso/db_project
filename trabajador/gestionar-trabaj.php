<!DOCTYPE html>
<html>
    <head>
        <title>Gestionar trabajadores HERR-APP</title>
        <link rel="stylesheet" type="text/css" href="style.css" >
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="titulo">
            <div>
                <br>
                <h1 align="center" style="color: white">GESTIONAR TRABAJADORES</h1>
                <h2 align="center" style="color: white">Menu</h2>
            </div>

            <div class="scrollmenu">
                <a href="/db-project/index.html">Inicio</a>
                <a href="/db-project/trabajador/FORMS/registrar-trabaj-form.html">Registrar trabajador</a>
                <a href="/db-project/trabajador/FORMS/eliminar-trabaj-form.php">Eliminar trabajador</a>
                <a href="/db-project/trabajador/FORMS/buscar-trabaj-form.php">Buscar</a>
            </div>
        </div>
        <div>
            <?php
                include($_SERVER['DOCUMENT_ROOT']."/db-project/trabajador/CRUD/trabaj-service.php");
                $nuevo = new Admin_Service();
                $nuevo -> mostrar_admin();
            ?>
        </div>
    </body>
</html>
